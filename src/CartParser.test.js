import CartParser from './CartParser';
import fs from 'fs';
import path from 'path';
import * as uuid from 'uuid';

jest.mock('fs');
jest.mock('uuid');

let parser, parse, validate, calcTotal, parseLine;

beforeEach(() => {
	parser = new CartParser();
	calcTotal = parser.calcTotal;
	parseLine = parser.parseLine.bind(parser);
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	const data = `Product name,Price,Quantity
			  Mollis consequat,9.00,2
			  Tvoluptatem,10.32,1`;
	const result = {
		"items": [
			{
				"id": "testID",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			},
			{
				"id": "testID",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			},
		],
		"total": 28.32
	}
	beforeEach(() => {
		fs.readFileSync.mockReturnValue(data);
		uuid.v4.mockReturnValue('testID');
	})

	describe('parse', () => {
		it('should return JSON object with correct data, when content is valid', () => {
			expect(parse()).toEqual(result);
		});

		it('should return Error, when content isn\`t valid', () => {
			const data = `AnotherField name,Price,Quantity
						  Mollis consequat,9.00,2
						  Tvoluptatem,10.32,1`;
			fs.readFileSync.mockReturnValue(data);
			expect(() => parse()).toThrow();
		});
	});
	describe('validate', () => {
		it('should return empty error array, when content is valid', () => {
			expect(validate(data)).toEqual([]);
		})

		it('should return array with error, when received header doesn\`t match expected one', () => {
			const data = `Product name,PriceHeader,Quantity
			  				Mollis consequat,9.00,2
							  Tvoluptatem,10.32,1`;
			expect(validate(data)).toEqual([{
				"column": 1,
				"message": "Expected header to be named \"Price\" but received PriceHeader.",
				"row": 0,
				"type": "header",
			}])
		})

		it('should return array with error, when received row cells length doesn\`t match expected one', () => {
			const data = `Product name,Price,Quantity
			Mollis consequat,9.00
			Tvoluptatem,10.32,1`;
			expect(validate(data)).toEqual([{
				"column": -1,
				"message": "Expected row to have 3 cells but received 2.",
				"row": 1,
				"type": "row",
			}])
		})

		it('should return array with error, when received cell is empty string', () => {
			const data = `Product name,Price,Quantity
			  Mollis consequat,9.00,2
			  ,10.32,1`;
			expect(validate(data)).toEqual([{
				"column": 0,
				"message": "Expected cell to be a nonempty string but received \"\".",
				"row": 2,
				"type": "cell",
			}]);
		})

		it('should return array with error, when received cell value is negative number', () => {
			const data = `Product name,Price,Quantity
			  Mollis consequat,9.00,2
			  Tvoluptatem,10.32,-1`;
			expect(validate(data)).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"-1\".",
				"row": 2,
				"type": "cell",
			}]);
		})

		it('should return array with error, when received cell value is NaN', () => {
			const data = `Product name,Price,Quantity
			  Mollis consequat,9.00,2
			  Tvoluptatem,10.32,NaN`;
			expect(validate(data)).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"NaN\".",
				"row": 2,
				"type": "cell",
			}]);
		})

		it('should return array with error, when received cell is not type of number', () => {
			const data = value => (`Product name,Price,Quantity
			  Mollis consequat,9.00,2
			  Tvoluptatem,10.32, ${value}`);

			expect(validate(data("abc"))).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"abc\".",
				"row": 2,
				"type": "cell",
			}]);

			expect(validate(data(undefined))).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"undefined\".",
				"row": 2,
				"type": "cell",
			}]);

			expect(validate(data(null))).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"null\".",
				"row": 2,
				"type": "cell",
			}]);

			expect(validate(data(true))).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"true\".",
				"row": 2,
				"type": "cell",
			}]);

			expect(validate(data({}))).toEqual([{
				"column": 2,
				"message": "Expected cell to be a positive number but received \"[object Object]\".",
				"row": 2,
				"type": "cell",
			}]);
		})
	});
	describe('parseLine', () => {
		it('should return object with correct keys and fields', () => {
			const csvLine = 'Tvoluptatem,10.32,1';
			expect(parseLine(csvLine)).toEqual({
				"id": "testID",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			})
		})
	})
	
	describe('calcTotal', () => {
		it('should return correct total price', () => {
			const cartItems = result.items;
			expect(calcTotal(cartItems)).toBeCloseTo(28.32, 2)
		})
	})
});

describe('CartParser - integration test', () => {
	// Add your integration test here.

	it('should return JSON object with correct data', () => {
		fs.readFileSync = require.requireActual('fs').readFileSync;
		const dataFilePath = path.resolve(`${__dirname}/../samples/cart.csv`)
		expect(parser.parse(dataFilePath)).toEqual({
			"items": [
				{
					"id": "testID",
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": "testID",
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": "testID",
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				},
				{
					"id": "testID",
					"name": "Consectetur adipiscing",
					"price": 28.72,
					"quantity": 10
				},
				{
					"id": "testID",
					"name": "Condimentum aliquet",
					"price": 13.9,
					"quantity": 1
				}
			],
			"total": 348.32
		})
	})
});